<?php
/**
 * @file
 * Include file for pages related to the user's password.
 */

/**
 * Form builder; Change the user password.
 */
function passwordflow_change_password(&$form_state, $account) {
  $form['uid'] = array(
    '#type' => 'hidden',
    '#value' => $account->uid,
  );
  $form['password'] = array(
    '#type' => 'fieldset',
  );
  $form['password']['intro'] = array(
    '#type' => 'markup',
    '#prefix' => '<p class="forms-intro">',
    '#suffix' => '</p>',
    '#value' => variable_get('passwordflow_change_password_description', t('To create your new password, please enter it in the fields below:')),
  );
  $form['password']['password'] = array(
    '#type' => 'password_confirm',
    '#required' => TRUE,
    '#ec_remove_colon' => TRUE,
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Change password'));
  $form['#redirect'] = t(variable_get('passwordflow_changed_password_redirect_path', '<front>'), array('!user_edit' => "user/$account->uid/edit"));
  return $form;
}

/**
 * Submit function for the change password form.
 */
function passwordflow_change_password_submit($form, &$form_state) {
  $edit = array('pass' => $form_state['values']['password']);
  $account = new stdClass();
  $account->uid = $form_state['values']['uid'];
  user_save($account, $edit);

  drupal_set_message(variable_get('passwordflow_changed_password_confirmation_message', t('Your password has been changed successfully.')));
  return;
}
