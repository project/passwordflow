<?php
// $Id$

/**
 * @file
 * Implementation of the administration pages of the module.
 */

/**
 * Form builder, configuration options for passwordflow.
 *
 * @see system_settings_form()
 */
function passwordflow_settings_form() {
$form = array();
  $form['passwordflow_seperate_password_page'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add a seperate page for changing the password.'),
    '#default_value' => variable_get('passwordflow_seperate_password_page', TRUE),
    '#description' => t('Enabling this option removes the password textboxes from the user edit page and moves them to a seperate page.'),
    '#element_validate' => array('passwordflow_validate_main_option'),
  );
  if (variable_get('passwordflow_seperate_password_page', TRUE)) {
    $form['change'] = array(
      '#type' => 'fieldset',
      '#title' => t('Change password options'),
      '#description' => t('Configure where to display the change password link.')
    );
    $form['change']['passwordflow_change_as_local_task'] = array(
      '#type' => 'checkbox',
      '#title' => t('Make change password a tab on user pages'),
      '#default_value' => variable_get('passwordflow_change_as_local_task', FALSE),
      '#description' => t('Enabling this option will make change password a MENU_LOCAL_TASK so it appears as a tab on user pages, like view and edit.'),
    );
    $form['change']['passwordflow_change_password_as_user_category'] = array(
      '#type' => 'checkbox',
      '#title' => t('Make change password a user category'),
      '#default_value' => variable_get('passwordflow_change_password_as_user_category', FALSE),
      '#description' => t('Enabling this option will make change password a user category appearing on the user edit page under the tabs.'),
    );
    $form['change']['passwordflow_change_password_link_on_account_page'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show change password link on user edit page'),
      '#default_value' => variable_get('passwordflow_change_password_link_on_account_page', TRUE),
      '#description' => t('Enabling this option will add a change password link on the user edit page in place of the password textboxes.'),
    );
    $form['change']['passwordflow_remove_one_time_link_message'] = array(
      '#type' => 'checkbox',
      '#title' => t('Remove the used one time link message'),
      '#default_value' => variable_get('passwordflow_remove_one_time_link_message', TRUE),
      '#description' => t('Enabling this option will remove the message telling the user about the one time link, which was only needed to make sure the user changed the password on the user edit page.'),
    );
    $form['change']['passwordflow_change_password_description'] = array(
      '#type' => 'textarea',
      '#title' => t('Change password form description.'),
      '#default_value' => variable_get('passwordflow_change_password_description', 'To create your new password please enter it in the fields below.'),
      '#description' => t('Description text to display on the change password page.'),
    );
    $form['change']['passwordflow_changed_password_confirmation_message'] = array(
      '#type' => 'textfield',
      '#title' => t('Confirmation message'),
      '#default_value' => variable_get('passwordflow_changed_password_confirmation_message', 'Your password has been changed successfully.'),
      '#description' => t('The text to display as a confirmation message once the password has been changed.'),
    );
    $form['change']['passwordflow_changed_password_redirect_path'] = array(
      '#type' => 'textfield',
      '#title' => t('Redirect on success path'),
      '#default_value' => variable_get('passwordflow_changed_password_redirect_path', '<front>'),
      '#description' => t('The path to redirect to once the password has successfully been changed. !user_edit is available as a variable to redirect to the user edit page.'),
    );
  }

  $form['reset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Reset options'),
  );

  $form['reset']['passwordflow_remove_additional_reset_step'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove additional step in password reset flow'),
    '#default_value' => variable_get('passwordflow_remove_additional_reset_step', TRUE),
    '#description' => t('By default when you click on the link send via email, you then get to a page where you have to click login, enabling this option will skip that unnecessary step.'),
  );

  return system_settings_form($form);

}

/**
 * Custom element validate function.
 *
 * @param $form
 * @param $form_state
 */
function passwordflow_validate_main_option($form, &$form_state) {
  // If we don't have a seperate password page make sure none of the dependent
  // options are selected.
  if (!$form_state['values']['passwordflow_seperate_password_page']) {
    $form_state['values']['passwordflow_change_as_local_task'] = FALSE;
    $form_state['values']['passwordflow_change_password_as_user_category'] = FALSE;
    $form_state['values']['passwordflow_change_password_link_on_account_page'] = FALSE;
    $form_state['values']['passwordflow_change_password_description'] = FALSE;
    $form_state['values']['passwordflow_changed_password_confirmation_message'] = FALSE;
    unset($form_state['values']['passwordflow_changed_password_redirect_path']);
  }
}